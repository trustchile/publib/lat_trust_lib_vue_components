import { VueConstructor } from 'vue';
import DGrid from "./components/DGrid.vue";
import IObserver from "./components/IObserver.vue";
import DText from "./components/DText.vue";
import DTransition from "./components/DTransition.vue";


export default function install(Vue: VueConstructor) {
  //@ts-ignore
  if (install.installed) return;

  //@ts-ignore
  install.installed = true;

  Vue.component("d-grid", DGrid);
  Vue.component("d-text", DText);
  Vue.component("d-transition", DTransition);
  Vue.component("i-observer", IObserver);
}

if (typeof window !== "undefined" && "Vue" in window) {
  //@ts-ignore
  window.Vue.use({ install });
}